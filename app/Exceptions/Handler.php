<?php

namespace App\Exceptions;

use Exception;
use App;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Validation\ValidationException as ValidationException;
use Illuminate\Auth\AuthenticationException as AuthenticationException;
use Illuminate\Auth\AuthorizationException  as AuthorizationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException as HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //dd(new AccessDeniedHttpException);
        //dd($exception);
        //if ($exception instanceof HttpException) dd($exception->getStatusCode());  
        if ($exception instanceof AccessDeniedHttpException) {
            dd($exception);
        }  
        //dd($request->request->all());
        //dd($this);
        
        // Json error exceptions
        if ($request->wantsJson())
        {
            // 404 Not found
            if ($exception instanceof ModelNotFoundException)
            {
                return response()->json([
                    'code' => 404,
                    'message' => 'Resource not found.'
                ], 404);
            }
            // 422 Unprocessable Entity
            if ($exception instanceof ValidationException)
            {
                return response()->json([
                    'code' => $exception->status,
                    'message' => 'The given data was invalid.',
                    'errors' => $exception->errors(),
                ],  $exception->status);
            }
            // 403 Forbidden
            if ($exception instanceof AccessDeniedHttpException)
            {
                return response()->json([
                    'code' => 403,
                    'message' => 'Access denied.',
                ],403);
            }
            // 401 Unauthorized
            if ($exception instanceof AuthenticationException)
            {
                return response()->json([
                    'code' => 401,
                    'message' => 'Unauthenticated error.',
                ],  401);
            }
        }

        return parent::render($request, $exception);
    }

}


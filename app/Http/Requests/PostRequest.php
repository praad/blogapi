<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
//use Illuminate\Support\Facades\Auth;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd(Auth::check());
        // By default it returns false, change it to something like
        // this if ure using auth
        //return Auth::check();

        //dd(Auth::user()->id);
        //dd(request()->request->parameters);
        $request = request()->request->all();
        if ($request['author'] == Auth::user()->id) {
            //return false;
            return true;
        }
        //403
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'slug' => 'required',
            //'author' => 'required',
            //'title' => 'required|unique:posts|max:255',
            'title' => 'required',
            'body' => 'required',
        ];
    }
}
